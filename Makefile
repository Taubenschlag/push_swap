NAME = push_swap
ASAN = push_asan

CC = gcc
FLAGS = -Wall -Wextra -Werror
AFLAG = -g -fsanitize=address
LIB = -lft -L$(LFT_DIR)
INCL = -I$(HEAD_DIR) -I$(LFT_DIR)

LFT_DIR = ./libft/
LFT_HEAD =  $(LFT_DIR)libft.h
LIBFT = $(LFT_DIR)libft.a

HEAD_DIR = ./include/
HEAD_F = push_swap.h
HEADS = $(addprefix $(HEAD_DIR), $(HEAD_F))

SRC_DIR = ./src/
SRC_F = handle_input.c           nonrotative_rr.c           stack_init_ops.c\
        main.c                   nonrotative_rrr.c          stack_sort_ops_1.c\
        make_chunk.c             nonrotative_stack_ops_1.c  stack_sort_ops_2.c\
        multichunk_deep_merge.c  one_chunk_op.c             stack_sort_ops.c\
        multichunk_merge.c       one_chunk_sort.c           utils.c\
        multichunk_op.c          ps_init.c\
        multichunk_sort.c        sorted_chunked_check.c

SRCS = $(addprefix $(SRC_DIR), $(SRC_F))

OBJ_DIR = ./tmp/
OBJ_LIST = $(patsubst %.c, %.o, $(SRC_F))
OBJS = $(addprefix $(OBJ_DIR), $(OBJ_LIST))

.PHONY: all clean fclean re

all: $(NAME)

$(NAME): $(LIBFT) $(OBJ_DIR) $(OBJS) 
	$(CC) $(OBJS) $(FLAGS) $(LIB) $(INCL) -o $(NAME)

$(OBJ_DIR):
	mkdir -p $(OBJ_DIR)

$(OBJ_DIR)%.o : $(SRC_DIR)%.c $(HEADS)
	$(CC) -c $< $(FLAGS) $(INCL) -o $@

$(LIBFT):
	$(MAKE) -sC $(LFT_DIR)

clean:
	@$(MAKE) -sC $(LFT_DIR) clean
	@rm -rfd $(OBJ_DIR)*

fclean: clean
	@rm -rfd $(LIBFT)
	@rm -rfd $(NAME)

re:
	@$(MAKE) fclean
	@$(MAKE) all

