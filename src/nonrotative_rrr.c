#include <push_swap.h>

void	nrrr_copy_meta(int **new_meta, t_chunk *d, int base, int **ret)
{
	*ret = malloc(sizeof(int) * d->inp_len);
	*new_meta = malloc(sizeof(int) * METAL);
	(*new_meta)[LEN_A] = d->meta[base][LEN_A] + 1;
	(*new_meta)[LEN_B] = d->meta[base][LEN_B] + 1;
	(*new_meta)[LEN_C] = d->meta[base][LEN_C] - 1;
	(*new_meta)[LEN_D] = d->meta[base][LEN_D] - 1;
	ft_memcpy(*ret, d->state_log[base], d->inp_len * sizeof(int));
}

void	fill_first_rrr(int *loc_itr, t_chunk *d, int base, int *ret)
{
	while (loc_itr[1] < d->meta[base][LEN_A] + d->meta[base][LEN_C])
	{
		ret[loc_itr[1]] = d->state_log[base][loc_itr[0]];
		loc_itr[0]++;
		loc_itr[1]++;
	}
}

int	*n_rrr(t_chunk *d, int base, int **meta)
{
	int	*ret;
	int	*new_meta;
	int	*loc_itr;

	loc_itr = malloc(sizeof(int) * 2);
	nrrr_copy_meta(&new_meta, d, base, &ret);
	loc_itr[0] = 0;
	loc_itr[1] = d->meta[base][LEN_A] + d->meta[base][LEN_C] - 1;
	ret[0] = d->state_log[base][loc_itr[1]];
	loc_itr[1] = 1;
	fill_first_rrr(loc_itr, d, base, ret);
	loc_itr[0] = d->meta[base][LEN_A] + d->meta[base][LEN_C];
	loc_itr[1] = loc_itr[0] + 1;
	ret[loc_itr[0]] = d->state_log[base][d->inp_len - 1];
	while (loc_itr[1] < d->inp_len)
	{
		ret[loc_itr[1]] = d->state_log[base][loc_itr[0]];
		loc_itr[0]++;
		loc_itr[1]++;
	}
	set_lap(d, ret, new_meta, meta);
	free(loc_itr);
	return (ret);
}
