#include <push_swap.h>

char	**make_empty_res(void)
{
	char	**res;

	res = malloc(sizeof(char *));
	res[0] = NULL;
	return (res);
}

void	res_and_free(char ***res, t_chunk *c)
{
	int	redundant;

	*res = make_result(c, &redundant);
	free_chunk(c);
	free(c);
}

char	**ch_sort(t_chunk *c)
{
	int		op_enum;
	int		base;
	char	**res;

	base = 0;
	if (!is_sorted(c->state_log[0], c->meta[0]))
	{
		while (base < c->log_len)
		{
			while (c->inp_len > 8 && c->meta[base][LAP_LEN] < c->lap_max - 2)
				base++;
			op_enum = SA;
			while (op_enum <= RB)
			{
				if (try_perform(c, op_enum, base))
				{
					res_and_free(&res, c);
					return (res);
				}
				op_enum++;
			}
			base++;
		}
	}
	return (make_empty_res());
}
