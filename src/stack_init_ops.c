#include <push_swap.h>

int	check_dup_state(t_chunk *d, int *res, int *m, int h)
{
	int	i;
	int	j;

	i = 0;
	while (i < d->hash_len[h])
	{
		if (m[LEN_A] == d->hsh_mt[h][i][LEN_A]
			&& m[LEN_C] == d->hsh_mt[h][i][LEN_C]
			&& m[LEN_D] == d->hsh_mt[h][i][LEN_D]
			&& m[LEN_B] == d->hsh_mt[h][i][LEN_B])
		{
			j = 0;
			while (j < d->inp_len)
			{
				if (res[j] == d->hsh_st[h][i][j])
					j++;
				else
					break ;
			}
			if (j == d->inp_len)
				return (0);
		}
		i++;
	}
	return (1);
}

int	*grab_n_op(t_chunk *d, int op, int b, int **m)
{
	if (op == SA && d->meta[b][LEN_A] > 1 && **m != SA)
		return (sa(d, b, m));
	if (op == SB && d->meta[b][LEN_B] > 1 && **m != SB)
		return (sb(d, b, m));
	if (op == SS && d->meta[b][LEN_B] > 1 && d->meta[b][LEN_A] > 1 && **m != SS)
		return (ss(d, b, m));
	if (op == RA && d->meta[b][LEN_A] > 0 && **m != RRA)
		return (n_ra(d, b, m));
	if (op == RB && d->meta[b][LEN_B] > 0 && **m != RRB)
		return (n_rb(d, b, m));
	if (op == RR && d->meta[b][LEN_B]
		> 0 && d->meta[b][LEN_A] > 0 && **m != RRR)
		return (n_rr(d, b, m));
	if (op == RRA && d->meta[b][LEN_C] > 0 && **m != RA)
		return (n_rra(d, b, m));
	if (op == RRB && d->meta[b][LEN_D] > 0 && **m != RB)
		return (n_rrb(d, b, m));
	if (op == RRR && d->meta[b][LEN_C]
		> 0 && d->meta[b][LEN_D] > 0 && **m != RR)
		return (n_rrr(d, b, m));
	if (op == PA && d->meta[b][LEN_B] > 0 && **m != PB)
		return (pa(d, b, m));
	if (op == PB && d->meta[b][LEN_A] > 0 && **m != PA)
		return (pb(d, b, m));
	return (NULL);
}

int	try_perform(t_chunk *d, int op, int b)
{
	int	*res;
	int	*meta;
	int	prv;
	int	prv_lap;

	prv = NON;
	prv_lap = d->lap_max_final;
	if (b)
	{
		prv = d->op_log[b][d->meta[b][OPS_L] - 1];
		prv_lap = d->meta[b][LAP_LEN];
	}
	meta = &prv;
	if (d->number == -1)
		res = grab_op(d, op, b, &meta);
	else
		res = grab_n_op(d, op, b, &meta);
	if (!check_nores(d, res, meta, prv_lap))
		return (0);
	meta[OPS_L] = op;
	if (do_op(d, res, meta, b))
		return (1);
	return (0);
}
