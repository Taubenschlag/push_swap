#include <push_swap.h>

int	*rra(t_chunk *d, int base, int **meta)
{
	int	*ret;
	int	*new_meta;
	int	i;
	int	j;

	copy_meta(&new_meta, d, base, &ret);
	i = 0;
	j = 1;
	ret[0] = d->state_log[base][new_meta[LEN_A] - 1];
	while (j < d->meta[base][LEN_A])
	{
		ret[j] = d->state_log[base][i];
		i++;
		j++;
	}
	set_lap(d, ret, new_meta, meta);
	return (ret);
}

int	*rrb(t_chunk *d, int base, int **meta)
{
	int	*ret;
	int	*new_meta;
	int	i;
	int	j;

	copy_meta(&new_meta, d, base, &ret);
	i = d->meta[base][LEN_A];
	j = i + 1;
	ret[i] = d->state_log[base][d->inp_len - 1];
	while (j < d->inp_len)
	{
		ret[j] = d->state_log[base][i];
		i++;
		j++;
	}
	set_lap(d, ret, new_meta, meta);
	return (ret);
}

int	*rrr(t_chunk *d, int base, int **meta)
{
	int	*ret;
	int	*new_meta;
	int	i;
	int	j;

	copy_meta(&new_meta, d, base, &ret);
	i = 0;
	j = 1;
	ret[0] = d->state_log[base][new_meta[LEN_A] - 1];
	while (j < d->meta[base][LEN_A])
	{
		ret[j] = d->state_log[base][i];
		i++;
		j++;
	}
	ret[j++] = d->state_log[base][d->inp_len - 1];
	i++;
	while (j < d->inp_len)
	{
		ret[j] = d->state_log[base][i];
		i++;
		j++;
	}
	set_lap(d, ret, new_meta, meta);
	return (ret);
}
