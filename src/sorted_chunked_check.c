#include <push_swap.h>

int	check_chunked_b(int *meta, int *p, int max)
{
	int	i;

	i = 1;
	while (i < meta[LEN_B])
	{
		if (p[i] < max)
			return (0);
		max = p[i];
		i++;
	}
	return (1);
}

int	check_chunked_c(int *meta, int *p, int max)
{
	int	i;

	i = 1;
	while (i < meta[LEN_C])
	{
		if (p[i] < max)
			return (0);
		max = p[i];
		i++;
	}
	return (1);
}

int	is_chunked(int *res, int *meta, int n)
{
	int	*p;
	int	max;

	if (!meta[LEN_A] && !meta[LEN_D])
	{
		p = res;
		max = p[0];
		if ((n % 2) && !meta[LEN_C])
			return (check_chunked_b(meta, p, max));
		else if (!(n % 2) && !meta[LEN_B])
			return (check_chunked_c(meta, p, max));
	}
	return (0);
}

int	is_sorted(int *res, int *meta)
{
	int	i;
	int	j;

	if (!meta[LEN_B])
	{
		i = meta[LEN_A] - 1;
		j = 0;
		while (i >= 0)
		{
			if (res[i] != i)
				return (0);
			i--;
			j++;
		}
		return (1);
	}
	return (0);
}
