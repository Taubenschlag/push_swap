#include <push_swap.h>

void	set_lap(t_chunk *d, int *ret, int *new_meta, int **meta)
{
	if (d->number == -1)
		new_meta[LAP_LEN] = lap(ret, new_meta[LEN_A],
				&(new_meta[LAP_BEG]),
				&(new_meta[LAP_VAL]));
	else if (d->number % 2)
		new_meta[LAP_LEN] = lap(ret + new_meta[LEN_A] + new_meta[LEN_C],
				new_meta[LEN_B],
				&(new_meta[LAP_BEG]),
				&(new_meta[LAP_VAL]));
	else if (!(d->number % 2))
		new_meta[LAP_LEN] = lap(ret + new_meta[LEN_A],
				new_meta[LEN_C],
				&(new_meta[LAP_BEG]),
				&(new_meta[LAP_VAL]));
	*meta = new_meta;
}

void	copy_meta(int **new_meta, t_chunk *d, int base, int **ret)
{
	*ret = malloc(sizeof(int) * d->inp_len);
	*new_meta = malloc(sizeof(int) * METAL);
	(*new_meta)[LEN_A] = d->meta[base][LEN_A];
	(*new_meta)[LEN_B] = d->meta[base][LEN_B];
	(*new_meta)[LEN_C] = d->meta[base][LEN_C];
	(*new_meta)[LEN_D] = d->meta[base][LEN_D];
	ft_memcpy(*ret, d->state_log[base], d->inp_len * sizeof(int));
}

int	*sa(t_chunk *d, int base, int **meta)
{
	int	*ret;
	int	*new_meta;
	int	*p1;
	int	*p2;

	copy_meta(&new_meta, d, base, &ret);
	if (d->meta[base][LEN_A] > 1)
	{
		p1 = &(ret[0]);
		p2 = &(ret[1]);
		ft_swap(p1, p2);
	}
	set_lap(d, ret, new_meta, meta);
	return (ret);
}

int	*sb(t_chunk *d, int base, int **meta)
{
	int	*ret;
	int	*new_meta;
	int	*p1;
	int	*p2;

	copy_meta(&new_meta, d, base, &ret);
	if (d->meta[base][LEN_B] > 1)
	{
		p1 = &(ret[d->meta[base][LEN_A] + d->meta[base][LEN_C]]);
		p2 = &(ret[d->meta[base][LEN_A] + d->meta[base][LEN_C] + 1]);
		ft_swap(p1, p2);
	}
	set_lap(d, ret, new_meta, meta);
	return (ret);
}

int	*ss(t_chunk *d, int base, int **meta)
{
	int	*ret;
	int	*new_meta;
	int	*p1;
	int	*p2;

	copy_meta(&new_meta, d, base, &ret);
	if (d->meta[base][LEN_B] > 1)
	{
		p1 = &(ret[d->meta[base][LEN_A] + d->meta[base][LEN_C]]);
		p2 = &(ret[d->meta[base][LEN_A] + d->meta[base][LEN_C] + 1]);
		ft_swap(p1, p2);
	}
	if (d->meta[base][LEN_A] > 1)
	{
		p1 = &(ret[0]);
		p2 = &(ret[1]);
		ft_swap(p1, p2);
	}
	set_lap(d, ret, new_meta, meta);
	return (ret);
}
