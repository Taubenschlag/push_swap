#include <push_swap.h>

void	make_chunx(t_data *d, int size_rem, int *prep_inp, int passed_size)
{
	int	i;
	int	size;
	int	*chunky_meta;
	int	*chunky_input;

	i = 0;
	while (i < d->chunks_amount)
	{
		size = d->chunk_size;
		if (size_rem)
			size++;
		prepare_input_meta(prep_inp + passed_size,
			size, &chunky_input, &chunky_meta);
		chunky_meta[LAP_LEN] = 0;
		chunky_meta[LAP_VAL] = 0;
		chunky_meta[LAP_BEG] = 0;
		d->chunks[i] = one_chunk(chunky_input, prep_inp + passed_size,
				chunky_meta);
		passed_size += size;
		if (size_rem)
			size_rem--;
		free(chunky_meta);
		d->chunks[i]->number = i;
		i++;
	}
}

t_data	*multichunk(int *prepared_inp, int *meta_zero)
{
	int		size_remainder;
	int		passed_size;
	t_data	*d;

	d = malloc(sizeof(t_data));
	d -> chunks_amount = 1;
	d -> chunk_size = 0xffff;
	d -> fin_op_count = 0;
	while (d->chunk_size >= EF_CH_SZ)
	{
		d->chunks_amount *= 2;
		d->chunk_size = meta_zero[LEN_A] / d->chunks_amount;
	}
	size_remainder = meta_zero[LEN_A] - d->chunks_amount * d->chunk_size;
	d->chunks = malloc(sizeof(t_chunk *) * d->chunks_amount);
	passed_size = 0;
	make_chunx(d, size_remainder, prepared_inp, passed_size);
	d->results_tmp = malloc(sizeof(char **) * d->chunks_amount);
	return (d);
}
