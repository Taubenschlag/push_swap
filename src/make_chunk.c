#include <push_swap.h>

static void	allocate(t_chunk *c)
{
	int	i;

	i = 0;
	c->op_log = malloc(sizeof (int *) * c->log_cap);
	c->state_log = malloc(sizeof (int *) * c->log_cap);
	c->meta = malloc(sizeof (int *) * c->log_cap);
	c->hsh_st = malloc(sizeof (int **) * HSH_CAP);
	c->hsh_mt = malloc(sizeof (int **) * HSH_CAP);
	c->op_log[0] = malloc(sizeof(int ));
	c->op_log[0][0] = NON;
	c->meta[0] = malloc(sizeof(int) * METAL);
	while (i < HSH_CAP)
	{
		c->hsh_st[i] = NULL;
		c->hsh_mt[i] = NULL;
		c->hash_len[i] = 0;
		i++;
	}
}

t_chunk	*one_chunk(int *work_inp, int *prepared_inp, int *meta_zero)
{
	int		hsh;
	t_chunk	*c;

	c = malloc(sizeof(t_chunk));
	c->init_arr = malloc(sizeof(int ) * meta_zero[LEN_A]);
	ft_memcpy(c->init_arr, prepared_inp, sizeof(int ) * meta_zero[LEN_A]);
	c->log_len = 1;
	c->log_cap = LOG_CAP;
	c->inp_len = meta_zero[LEN_A];
	allocate(c);
	c->state_log[0] = work_inp;
	ft_memcpy(c->meta[0], meta_zero, METAL * sizeof(int));
	c->lap_max = meta_zero[LAP_LEN];
	c->lap_max_final = meta_zero[LAP_LEN];
	hsh = hash(c->state_log[0], c->meta[0], c->inp_len);
	c->hash_len[hsh]++;
	c->hsh_st[hsh] = malloc(sizeof(int *) * c->hash_len[hsh]);
	c->hsh_mt[hsh] = malloc(sizeof(int *) * c->hash_len[hsh]);
	c->hsh_st[hsh][c->hash_len[hsh] - 1] = c->state_log[0];
	c->hsh_mt[hsh][c->hash_len[hsh] - 1] = c->meta[0];
	c->number = -1;
	return (c);
}
