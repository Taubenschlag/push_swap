#include <push_swap.h>

void	prepare_new(t_m_ch **new, t_m_ch *a, t_m_ch *b, int count)
{
	new[count] = malloc(sizeof(t_m_ch));
	new[count]->len = a->len + b->len;
	new[count]->arr = malloc(sizeof(int) * new[count]->len);
	if (count % 2)
		new[count]->stack = 'b';
	else
		new[count]->stack = 'a';
}

char	**update_commands(t_data *d, int i, int j, int total)
{
	char	**new_res;

	i = 0;
	new_res = malloc(sizeof(char *) * (total + 1));
	while (i < d->fin_op_count)
	{
		new_res[i] = d->res_final[i];
		i++;
	}
	i = 0;
	while (d->results_tmp[i])
	{
		j = 0;
		while (d->results_tmp[i][j])
		{
			new_res[d->fin_op_count] = d->results_tmp[i][j];
			j++;
			(d->fin_op_count)++;
		}
		free(d->results_tmp[i]);
		i++;
	}
	free(d->results_tmp);
	free(d->res_final);
	return (new_res);
}

void	save_commands(t_data *d)
{
	int	i;
	int	j;
	int	total;

	i = 0;
	j = 0;
	total = 0;
	while (d->results_tmp[i])
	{
		j = 0;
		while (d->results_tmp[i][j])
		{
			j++;
			total++;
		}
		i++;
	}
	total += d->fin_op_count;
	d->res_final = update_commands(d, i, j, total);
	d->res_final[d->fin_op_count] = NULL;
}

void	free_old_merged(int i, t_data *d)
{
	free(d->merged_chunks[i]->arr);
	free(d->merged_chunks[i]);
	if (i + 1 < d->chunks_amount)
	{
		free(d->merged_chunks[i + 1]->arr);
		free(d->merged_chunks[i + 1]);
	}
}

void	merge(t_data *d)
{
	int		*l_i;
	t_m_ch	**new;

	new = malloc(sizeof(t_m_ch *) * (d->chunks_amount / 2));
	l_i = malloc(sizeof (int ) * 3);
	l_i[2] = 0;
	while (l_i[2] < d->chunks_amount)
	{
		d->counter = 0;
		ft_memset(l_i, 0, 2);
		prepare_new(new, d->merged_chunks[l_i[2]],
			d->merged_chunks[l_i[2] + 1], l_i[2] / 2);
		while (l_i[0] < d->merged_chunks[l_i[2]]->len
			|| l_i[1] < d->merged_chunks[l_i[2] + 1]->len)
		{
			merge_one_chunk(l_i[2], l_i, d, new);
			save_commands(d);
		}
		free_old_merged(l_i[2], d);
		l_i[2] += 2;
	}
	free(d->merged_chunks);
	free(l_i);
	d->merged_chunks = new;
	d->chunks_amount = d->chunks_amount / 2;
}
