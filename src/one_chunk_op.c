#include <push_swap.h>

void	hash_new_attempt(t_chunk *d, int h, int *res, int *meta)
{
	int	i;
	int	**st_log;
	int	**mt_log;

	i = 0;
	st_log = malloc(sizeof(int *) * d->hash_len[h]);
	mt_log = malloc(sizeof(int *) * d->hash_len[h]);
	while (d->hsh_st[h] && i < (d->hash_len[h] - 1))
	{
		st_log[i] = d->hsh_st[h][i];
		i++;
	}
	st_log[i] = res;
	if (d->hsh_st[h])
		free(d->hsh_st[h]);
	d->hsh_st[h] = st_log;
	i = -1;
	while (++i < (d->hash_len[h] - 1) && d->hsh_mt[h])
	{
		mt_log[i] = d->hsh_mt[h][i];
	}
	mt_log[i] = meta;
	if (d->hsh_mt[h])
		free(d->hsh_mt[h]);
	d->hsh_mt[h] = mt_log;
}

void	add_operation(t_chunk *d, int *meta, int base)
{
	int	*new_ops;
	int	i;
	int	op;

	i = 0;
	op = meta[OPS_L];
	new_ops = malloc(sizeof(int) * (d->meta[base][OPS_L] + 1));
	while (i < d->meta[base][OPS_L])
	{
		new_ops[i] = d->op_log[base][i];
		i++;
	}
	new_ops[i] = op;
	d->op_log[d->log_len] = new_ops;
}

int	do_op(t_chunk *d, int *res, int *meta, int base)
{
	int	h;

	add_operation(d, meta, base);
	h = hash(res, meta, d->inp_len);
	meta[OPS_L] = d->meta[base][OPS_L] + 1;
	d->state_log[d->log_len] = res;
	d->meta[d->log_len] = meta;
	d->hash_len[h]++;
	hash_new_attempt(d, h, res, meta);
	d->log_len++;
	if (meta[LAP_LEN] > d->lap_max)
		d->lap_max = meta[LAP_LEN];
	if (d->number == -1)
		return (is_sorted(res, meta));
	return (is_chunked(res, meta, d->number));
}

int	check_nores(t_chunk *d, int *res, int *meta, int prv_lap)
{
	int	h;

	if (!res)
		return (0);
	h = hash(res, meta, d->inp_len);
	if (d->hash_len[h] == 0)
		return (1);
	if (meta[LAP_LEN] < prv_lap
		|| (d->number == -1
			&& (d->inp_len > 8 && meta[LAP_LEN] < d->lap_max - 1))
		|| !check_dup_state(d, res, meta, h))
	{
		free(res);
		free(meta);
		return (0);
	}
	return (1);
}

int	*grab_op(t_chunk *d, int op, int b, int **m)
{
	if (op == SA && d->meta[b][LEN_A] > 1 && **m != SA)
		return (sa(d, b, m));
	if (op == SB && d->meta[b][LEN_B] > 1 && **m != SB)
		return (sb(d, b, m));
	if (op == SS && d->meta[b][LEN_B] > 1 && d->meta[b][LEN_A] > 1 && **m != SS)
		return (ss(d, b, m));
	if (op == RA && d->meta[b][LEN_A] > 1 && **m != RRA)
		return (ra(d, b, m));
	if (op == RB && d->meta[b][LEN_B] > 1 && **m != RRB)
		return (rb(d, b, m));
	if (op == RR && d->meta[b][LEN_B]
			> 1 && d->meta[b][LEN_A] > 1 && **m != RRR)
		return (rr(d, b, m));
	if (op == RRA && d->meta[b][LEN_A] > 1 && **m != RA)
		return (rra(d, b, m));
	if (op == RRB && d->meta[b][LEN_B] > 1 && **m != RB)
		return (rrb(d, b, m));
	if (op == RRR && d->meta[b][LEN_B]
			> 1 && d->meta[b][LEN_A] > 1 && **m != RR)
		return (rrr(d, b, m));
	if (op == PA && d->meta[b][LEN_B] > 0 && **m != PB)
		return (pa(d, b, m));
	if (op == PB && d->meta[b][LEN_A] > d->meta[b][LEN_B] && **m != PA)
		return (pb(d, b, m));
	return (NULL);
}
