#include <push_swap.h>

void	free_chunk(t_chunk *chunk)
{
	int	i;

	i = -1;
	while (++i < chunk->log_len)
	{
		free(chunk->state_log[i]);
		free(chunk->op_log[i]);
		free(chunk->meta[i]);
	}
	i = -1;
	while (++i < HSH_CAP)
	{
		if (chunk->hash_len[i])
		{
			free(chunk->hsh_st[i]);
			free(chunk->hsh_mt[i]);
		}
	}
	free(chunk->init_arr);
	free(chunk->hsh_st);
	free(chunk->hsh_mt);
	free(chunk->state_log);
	free(chunk->op_log);
	free(chunk->meta);
}

void	finalize_d_res(t_data *d)
{
	int	j;
	int	l;
	int	i;

	d->res_final = malloc(sizeof(char *) * d->fin_op_count + sizeof(int *));
	i = 0;
	l = 0;
	while (i < d->chunks_amount)
	{
		j = 0;
		while (d->results_tmp[i][j])
		{
			d->res_final[l] = d->results_tmp[i][j];
			j++;
			l++;
		}
		free(d->results_tmp[i]);
		i++;
	}
	free(d->results_tmp);
	d->res_final[l] = NULL;
}

char	**mutichunk_merge(t_data *d)
{
	int		i;
	t_m_ch	**sortd_mrgd_chnx;
	char	**ret;

	finalize_d_res(d);
	sortd_mrgd_chnx = malloc(sizeof(t_m_ch *) * d->chunks_amount);
	i = 0;
	while (i < d->chunks_amount)
	{
		sortd_mrgd_chnx[i] = d->merged_chunks[i];
		sortd_mrgd_chnx[i + 1] = d->merged_chunks[d->chunks_amount - i - 1];
		i += 2;
	}
	free(d->merged_chunks);
	d->merged_chunks = sortd_mrgd_chnx;
	while (d->chunks_amount != 1)
		merge(d);
	free(d -> merged_chunks[0]->arr);
	free(d -> merged_chunks[0]);
	free(d -> merged_chunks);
	ret = d->res_final;
	free(d);
	return (ret);
}

int	save_res(t_data *d, int i, t_chunk *c, int base)
{
	int	ops;

	d->results_tmp[i] = make_result(c, &ops);
	d->merged_chunks[i] = malloc(sizeof(t_m_ch));
	d->merged_chunks[i]->len = c->inp_len;
	d->merged_chunks[i]->arr = malloc(sizeof(int ) * c->inp_len);
	ft_memcpy(d->merged_chunks[i]->arr, c->init_arr,
		c->inp_len * sizeof(int));
	ft_sort(d->merged_chunks[i]->arr, d->merged_chunks[i]->len);
	if (c->meta[base][LEN_B])
		d->merged_chunks[i]->stack = 'b';
	else
		d->merged_chunks[i]->stack = 'a';
	d->fin_op_count += ops;
	free_chunk(c);
	free(c);
	return (1);
}

t_data	*multichunk_sort(t_data *d)
{
	int	op_enum;
	int	base;
	int	i;
	int	done;

	i = -1;
	d->merged_chunks = malloc(sizeof(t_m_ch *) * d->chunks_amount);
	while (++i < d->chunks_amount)
	{
		done = 0;
		base = 0;
		while (!done && base < d->chunks[i]->log_len)
		{
			op_enum = SA;
			 while (op_enum <= RB && !done)
			{
				if (try_perform(d->chunks[i], op_enum, base))
					done = save_res(d, i, d->chunks[i], base);
				op_enum++;
			}
			base++;
		}
	}
	free(d->chunks);
	return (d);
}
