#include <push_swap.h>

void	ft_swap(int *a, int *b)
{
	int	c;

	c = *a;
	*a = *b;
	*b = c;
}

void	set_lap_val(int *lap_val, int begin_fix, int l, int *res)
{
	if (l)
		*lap_val = res[begin_fix];
	else
		*lap_val = 0;
}

int	lap(int *res, int l, int *lap_beg, int *lap_val)
{
	int	ret;
	int	i;
	int	begin_fix;

	*lap_beg = 0;
	ret = 1;
	begin_fix = 0;
	while (*lap_beg < l)
	{
		i = 1;
		while (*lap_beg + i < l)
			if (res[*lap_beg + i] - res[*lap_beg] == i)
				i++;
		else
			break ;
		if (i >= ret)
		{
			ret = i;
			begin_fix = *lap_beg;
		}
		*lap_beg += i;
	}
	*lap_beg = begin_fix;
	set_lap_val(lap_val, begin_fix, l, res);
	return (ret);
}

int	hash(int *arr, int *meta, int l)
{
	long long	hsh;
	int			i;

	i = 0;
	hsh = 1;
	while (i < l)
	{
		hsh ^= ((hsh ^ i) ^ (hsh >> 19)) ^ ((hsh ^ arr[i]) ^ (hsh << 11));
		i++;
	}
	hsh ^= meta[LEN_A] ^ meta[LEN_C] ^ meta[LEN_D] ^ meta[LEN_B];
	i = (int)(hsh % HSH_CAP);
	if (i < 0)
		i *= -1;
	return (i);
}

int	get_value(int number, int *arr, int arr_len)
{
	int	i;

	i = 0;
	while (i < arr_len)
	{
		if (arr[i] == number)
			break ;
		i++;
	}
	return (i);
}
