#include <push_swap.h>

int	main(int ac, char **av)
{
	int		*input;
	char	**result;
	int		i;

	if (ac > 1)
	{
		if ((ac > 2 && check_input(av) && parse_input(&input, ac - 1, av))
			|| (ac == 2 && if_split(&ac, av, &input)))
		{
			result = calculate(input, ac - 1);
			i = 0;
			while (result[i])
			{
				ft_putendl_fd(result[i], 1);
				free (result[i]);
				i++;
			}
			free (result);
		}
		else
			ft_putendl_fd("Error", 2);
	}
	return (0);
}
