#include <push_swap.h>

int	check_input(char **av)
{
	int	i;
	int	j;

	i = 1;
	while (av[i])
	{
		j = 0;
		while (av[i][j])
		{
			while (av[i][j] == ' ')
			{
				j++;
			}
			if (ft_isdigit(av[i][j]) ||
				(av[i][j] == '-' &&
				 ((j > 0 && av[i][j - 1] == ' ') || j == 0) &&
				 ft_isdigit(av[i][j + 1])))
				j++;
			else
				return (0);
		}
		i++;
	}
	return (1);
}

int	parse_input(int **input, int len, char **av)
{
	int	i;
	int	j;
	int	err;

	i = -1;
	*input = malloc(sizeof(int) * len);
	err = 0;
	while (++i < len)
		(*input)[i] = ft_atoi(av[i + 1], &err);
	i = 0;
	while (i < len)
	{
		j = i + 1;
		while (j < len)
		{
			if ((*input)[i] == (*input)[j] || err)
			{
				free(*input);
				return (0);
			}
			j++;
		}
		i++;
	}
	return (1);
}

static void	free_split(char **split, int i)
{
	if (i == 1)
		free (split[0]);
	while (split[i])
	{
		free (split[i]);
		i++;
	}
	free (split[i]);
	free (split);
}

static void	split_to_av(char ***split)
{
	int		i;
	char	**new_s;
	char	**ptr;

	ptr = *split;
	i = 0;
	while (ptr[i])
		i++;
	new_s = malloc(sizeof(char *) * (i + 2));
	new_s[0] = NULL;
	while (i >= 0)
	{
		new_s[i + 1] = ptr[i];
		i--;
	}
	free (*split);
	*split = new_s;
}

int	if_split(int *ac, char **av, int **input)
{
	char	**splitted;
	int		i;
	int		ret;

	ret = 0;
	i = 0;
	if (*ac == 2)
		splitted = ft_split(av[1], ' ');
	else
		return (0);
	while (splitted[i])
		i++;
	split_to_av(&splitted);
	if (check_input(splitted) && parse_input(input, i, splitted))
		ret = 1;
	free_split(splitted, 1);
	*ac = i + 1;
	return (ret);
}
