#include <push_swap.h>

char	**move_rotate_b(t_m_ch **new, int i)
{
	char	**ret;

	if (new[i / 2]->stack == 'a')
	{
		ret = malloc(sizeof(char *) * 2);
		ret[0] = ft_strdup("ra");
		ret[1] = NULL;
	}
	else
	{
		ret = malloc(sizeof(char *) * 3);
		ret[0] = ft_strdup("pb");
		ret[1] = ft_strdup("rb");
		ret[2] = NULL;
	}
	return (ret);
}

char	**move_rotate_a(t_m_ch **new, int i)
{
	char	**ret;

	if (new[i / 2]->stack == 'a')
	{
		ret = malloc(sizeof(char *) * 3);
		ret[0] = ft_strdup("pa");
		ret[1] = ft_strdup("ra");
		ret[2] = NULL;
	}
	else
	{
		ret = malloc(sizeof(char *) * 2);
		ret[0] = ft_strdup("rb");
		ret[1] = NULL;
	}
	return (ret);
}

void	merge_one_chunk(int i, int *l, t_data *d, t_m_ch **new)
{
	t_m_ch	*a;
	t_m_ch	*b;
	int		res_subcount;

	a = d->merged_chunks[i];
	b = d->merged_chunks[i + 1];
	res_subcount = 0;
	d->results_tmp = malloc(sizeof(char **) * (a->len + b->len + 1));
	while (l[0] < a->len && (l[1] == b->len || (a->arr[l[0]] < b->arr[l[1]])))
	{
		d->results_tmp[res_subcount] = move_rotate_b(new, i);
		new[i / 2]->arr[d->counter] = d->merged_chunks[i]->arr[l[0]];
		l[0]++;
		d->counter++;
		d->results_tmp[++res_subcount] = NULL;
	}
	while (l[1] < b->len && (l[0] == a->len || (a->arr[l[0]] > b->arr[l[1]])))
	{
		d->results_tmp[res_subcount] = move_rotate_a(new, i);
		new[i / 2]->arr[d->counter] = d->merged_chunks[i + 1]->arr[l[1]];
		l[1]++;
		d->counter++;
		d->results_tmp[++res_subcount] = NULL;
	}
}
