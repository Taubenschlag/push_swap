#include <push_swap.h>

int	*pb(t_chunk *d, int base, int **meta)
{
	int	*ret;
	int	*new_meta;
	int	i;
	int	j;

	copy_meta(&new_meta, d, base, &ret);
	new_meta[LEN_A] = d->meta[base][LEN_A] - 1;
	new_meta[LEN_B] = d->meta[base][LEN_B] + 1;
	i = 0;
	j = 1;
	while (j < d->meta[base][LEN_A] + d->meta[base][LEN_C])
	{
		ret[i] = d->state_log[base][j];
		i++;
		j++;
	}
	ret[i] = d->state_log[base][0];
	while (j < d->inp_len)
	{
		ret[j] = d->state_log[base][j];
		j++;
	}
	set_lap(d, ret, new_meta, meta);
	return (ret);
}

int	*pa(t_chunk *d, int base, int **meta)
{
	int	*ret;
	int	*new_meta;
	int	i;
	int	j;

	copy_meta(&new_meta, d, base, &ret);
	new_meta[LEN_A] = d->meta[base][LEN_A] + 1;
	new_meta[LEN_B] = d->meta[base][LEN_B] - 1;
	i = 0;
	j = 1;
	ret[0] = d->state_log[base][d->meta[base][LEN_A] + d->meta[base][LEN_C]];
	while (i < d->meta[base][LEN_A] + d->meta[base][LEN_C])
	{
		ret[j] = d->state_log[base][i];
		i++;
		j++;
	}
	while (j < d->inp_len)
	{
		ret[j] = d->state_log[base][j];
		j++;
	}
	set_lap(d, ret, new_meta, meta);
	return (ret);
}

int	*ra(t_chunk *d, int base, int **meta)
{
	int	*ret;
	int	*new_meta;
	int	i;
	int	j;

	copy_meta(&new_meta, d, base, &ret);
	i = 0;
	j = 1;
	while (j < d->meta[base][LEN_A])
	{
		ret[i] = d->state_log[base][j];
		i++;
		j++;
	}
	ret[i] = d->state_log[base][0];
	set_lap(d, ret, new_meta, meta);
	return (ret);
}

int	*rb(t_chunk *d, int base, int **meta)
{
	int	*ret;
	int	*new_meta;
	int	i;
	int	j;

	copy_meta(&new_meta, d, base, &ret);
	i = d->meta[base][LEN_A];
	j = i + 1;
	while (j < d->inp_len)
	{
		ret[i] = d->state_log[base][j];
		i++;
		j++;
	}
	ret[i] = d->state_log[base][new_meta[LEN_A]];
	set_lap(d, ret, new_meta, meta);
	return (ret);
}

int	*rr(t_chunk *d, int base, int **meta)
{
	int	*ret;
	int	*new_meta;
	int	i;
	int	j;

	copy_meta(&new_meta, d, base, &ret);
	i = 0;
	j = 1;
	while (j < d->meta[base][LEN_A])
	{
		ret[i] = d->state_log[base][j];
		i++;
		j++;
	}
	ret[i++] = d->state_log[base][0];
	j++;
	while (j < d->inp_len)
	{
		ret[i] = d->state_log[base][j];
		i++;
		j++;
	}
	ret[i] = d->state_log[base][new_meta[LEN_A]];
	set_lap(d, ret, new_meta, meta);
	return (ret);
}
