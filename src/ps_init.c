#include <push_swap.h>

char	*str_by_op_num(t_chunk *d, int l, int i)
{
	if (d->op_log[l][i] == SA)
		return (ft_strdup("sa"));
	else if (d->op_log[l][i] == SB)
		return (ft_strdup("sb"));
	else if (d->op_log[l][i] == SS)
		return (ft_strdup("ss"));
	else if (d->op_log[l][i] == RA)
		return (ft_strdup("ra"));
	else if (d->op_log[l][i] == RB)
		return (ft_strdup("rb"));
	else if (d->op_log[l][i] == RR)
		return (ft_strdup("rr"));
	else if (d->op_log[l][i] == RRA)
		return (ft_strdup("rra"));
	else if (d->op_log[l][i] == RRB)
		return (ft_strdup("rrb"));
	else if (d->op_log[l][i] == RRR)
		return (ft_strdup("rrr"));
	else if (d->op_log[l][i] == PB)
		return (ft_strdup("pb"));
	else if (d->op_log[l][i] == PA)
		return (ft_strdup("pa"));
	return (NULL);
}

char	**make_result(t_chunk *d, int *ops_n)
{
	int		l;
	char	**ret;

	l = d->log_len - 1;
	ret = malloc(sizeof(char *) * (d->meta[l][OPS_L] + 1));
	*ops_n = 0;
	while (*ops_n < d->meta[l][OPS_L])
	{
		ret[*ops_n] = str_by_op_num(d, l, *ops_n);
		(*ops_n)++;
	}
	ret[*ops_n] = NULL;
	return (ret);
}

void	prepare_input_meta(int *input, int input_len, int **ret, int **meta)
{
	int	i;
	int	*sorted;

	i = 0;
	sorted = malloc(sizeof(int) * input_len);
	ft_memcpy(sorted, input, input_len * sizeof(int));
	ft_sort(sorted, input_len);
	*ret = malloc(sizeof(int) * input_len);
	while (i < input_len)
	{
		(*ret)[i] = get_value(input[i], sorted, input_len);
		i++;
	}
	free(sorted);
	*meta = malloc(METAL * sizeof(int));
	(*meta)[LAP_LEN] = lap(*ret, input_len, &((*meta)[LAP_BEG]),
			&((*meta)[LAP_VAL]));
	(*meta)[LEN_A] = input_len;
	(*meta)[LEN_B] = 0;
	(*meta)[LEN_C] = 0;
	(*meta)[LEN_D] = 0;
	(*meta)[OPS_L] = 0;
}

char	**calculate(int *input, int input_len)
{
	char	**res;
	int		*meta_zero;
	int		*prepared;
	int		*work_inp;

	work_inp = malloc(sizeof(int) * input_len);
	prepare_input_meta(input, input_len, &prepared, &meta_zero);
	ft_memcpy(work_inp, prepared, input_len * sizeof(int));
	if (input_len - meta_zero[LAP_LEN] < EF_UNS_S || input_len < EF_ON_CH)
		res = ch_sort(one_chunk(work_inp, prepared, meta_zero));
	else
	{
		res = mutichunk_merge(multichunk_sort(multichunk(prepared, meta_zero)));
		free(work_inp);
	}
	free(prepared);
	free(meta_zero);
	free(input);
	return (res);
}
