#include <push_swap.h>

int	*n_ra(t_chunk *d, int base, int **meta)
{
	int	*ret;
	int	*new_meta;
	int	i;
	int	j;

	copy_meta(&new_meta, d, base, &ret);
	new_meta[LEN_A] = d->meta[base][LEN_A] - 1;
	new_meta[LEN_C] = d->meta[base][LEN_C] + 1;
	i = 0;
	j = 1;
	while (j < d->meta[base][LEN_A] + d->meta[base][LEN_C])
	{
		ret[i] = d->state_log[base][j];
		i++;
		j++;
	}
	i = d->meta[base][LEN_A] + d->meta[base][LEN_C] - 1;
	ret[i] = d->state_log[base][0];
	set_lap(d, ret, new_meta, meta);
	return (ret);
}

int	*n_rb(t_chunk *d, int base, int **meta)
{
	int	*ret;
	int	*new_meta;
	int	i;
	int	j;

	copy_meta(&new_meta, d, base, &ret);
	new_meta[LEN_B] = d->meta[base][LEN_B] - 1;
	new_meta[LEN_D] = d->meta[base][LEN_D] + 1;
	i = d->meta[base][LEN_A] + d->meta[base][LEN_C];
	j = i + 1;
	while (j < d->inp_len)
	{
		ret[i] = d->state_log[base][j];
		i++;
		j++;
	}
	ret[i] = d->state_log[base][d->meta[base][LEN_A] + d->meta[base][LEN_C]];
	set_lap(d, ret, new_meta, meta);
	return (ret);
}

int	*n_rra(t_chunk *d, int base, int **meta)
{
	int	*ret;
	int	*new_meta;
	int	i;
	int	j;

	copy_meta(&new_meta, d, base, &ret);
	new_meta[LEN_A] = d->meta[base][LEN_A] + 1;
	new_meta[LEN_C] = d->meta[base][LEN_C] - 1;
	i = 0;
	j = d->meta[base][LEN_A] + d->meta[base][LEN_C] - 1;
	ret[0] = d->state_log[base][j];
	j = 1;
	while (j < d->meta[base][LEN_A] + d->meta[base][LEN_C])
	{
		ret[j] = d->state_log[base][i];
		i++;
		j++;
	}
	set_lap(d, ret, new_meta, meta);
	return (ret);
}

int	*n_rrb(t_chunk *d, int base, int **meta)
{
	int	*ret;
	int	*new_meta;
	int	i;
	int	j;

	copy_meta(&new_meta, d, base, &ret);
	new_meta[LEN_B] = d->meta[base][LEN_B] + 1;
	new_meta[LEN_D] = d->meta[base][LEN_D] - 1;
	i = d->meta[base][LEN_A] + d->meta[base][LEN_C];
	j = i + 1;
	ret[i] = d->state_log[base][d->inp_len - 1];
	while (j < d->inp_len)
	{
		ret[j] = d->state_log[base][i];
		i++;
		j++;
	}
	set_lap(d, ret, new_meta, meta);
	return (ret);
}
