/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rokupin <rokupin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 18:11:27 by rokupin           #+#    #+#             */
/*   Updated: 2021/11/05 21:23:37 by rokupin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	handle_num(const char *num, int negative, int *error)
{
	long	atoi;

	atoi = 0;
	while (*num >= '0' && *num <= '9')
	{
		atoi *= 10;
		atoi += *num - '0';
		num++;
		if (negative == -1 && atoi > 2147483648)
		{
			*error = 1;
			return (0);
		}
		else if (negative == 1 && atoi > 2147483647)
		{
			*error = 1;
			return (-1);
		}
	}
	return ((int)atoi * negative);
}

int	ft_atoi(const char *str, int *error)
{
	int		negative;
	char	*ptr;

	ptr = (char *)str;
	negative = 0;
	while (*ptr && ((*ptr >= 9 && *ptr <= 13) || *ptr == ' '))
		ptr++;
	if (*ptr == '-' || *ptr == '+')
	{
		if (*ptr == '-')
			negative++;
		ptr++;
	}
	if (negative > 0)
		negative = -1;
	else
		negative = 1;
	return (handle_num(ptr, negative, error));
}
