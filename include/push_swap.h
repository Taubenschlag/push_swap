#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# define LEN_A    0
# define LEN_C    1
# define LEN_B    2
# define LEN_D    3
# define OPS_L    4
# define LAP_LEN  5
# define LAP_BEG  6
# define LAP_VAL  7
# define METAL    8
# define NON      9
# define SA       10
# define RA       11
# define RRA      12
# define PB       13
# define SS       14
# define RR       15
# define RRR      16
# define PA       17
# define SB       18
# define RRB      19
# define RB       20
# define EF_CH_SZ 8
# define EF_ON_CH 17
# define EF_UNS_S 15
# define LOG_CAP  80000000
# define HSH_CAP  1000000

# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <libft.h>

typedef struct s_chunk
{
	int	*init_arr;
	int	log_cap;
	int	log_len;
	int	inp_len;
	int	lap_max;
	int	lap_max_final;
	int	**op_log;
	int	**state_log;
	int	**meta;
	int	hash_len[HSH_CAP];
	int	***hsh_st;
	int	***hsh_mt;
	int	number;
}		t_chunk;

typedef struct s_m_ch
{
	int		*arr;
	int		len;
	char	stack;
}		t_m_ch;

typedef struct s_data
{
	int		fin_op_count;
	char	***results_tmp;
	char	**res_final;
	int		chunk_size;
	int		chunks_amount;
	int		counter;
	t_chunk	**chunks;
	t_m_ch	**merged_chunks;
}			t_data;

int		if_split(int *ac, char **av, int **input);
int		parse_input(int **input, int len, char **av);
int		check_input(char **av);
void	prepare_input_meta(int *input, int input_len, int **ret, int **meta);

char	**calculate(int *input, int input_len);

t_chunk	*one_chunk(int *work_inp, int *prepared_inp, int *meta_zero);
char	**ch_sort(t_chunk *c);

t_data	*multichunk(int *prepared_inp, int *meta_zero);

char	**mutichunk_merge(t_data *d);
t_data	*multichunk_sort(t_data *d);
void	merge_one_chunk(int i, int *l_i, t_data *d, t_m_ch **new);
void	merge(t_data *d);
void	free_chunk(t_chunk *chunk);

int		lap(int *res, int l, int *lap_beg, int *lap_val);
int		hash(int *arr, int *meta, int l);
int		get_value(int number, int *arr, int arr_len);

int		do_op(t_chunk *d, int *res, int *meta, int base);
int		check_nores(t_chunk *d, int *res, int *meta, int prv_lap);
int		*grab_op(t_chunk *d, int op, int b, int **m);
int		check_dup_state(t_chunk *d, int *res, int *m, int h);
int		is_chunked(int *res, int *meta, int n);

void	ft_swap(int *a, int *b);
int		is_sorted(int *res, int *meta);
int		try_perform(t_chunk *d, int op, int b);
char	**make_result(t_chunk *d, int *ops_n);

void	set_lap(t_chunk *d, int *ret, int *new_meta, int **meta);
void	copy_meta(int **new_meta, t_chunk *d, int base, int **ret);
int		*sa(t_chunk *d, int base, int **meta);
int		*sb(t_chunk *d, int base, int **meta);
int		*ss(t_chunk *d, int base, int **meta);
int		*pa(t_chunk *d, int base, int **meta);
int		*pb(t_chunk *d, int base, int **meta);
int		*ra(t_chunk *d, int base, int **meta);
int		*rb(t_chunk *d, int base, int **meta);
int		*rr(t_chunk *d, int base, int **meta);
int		*rra(t_chunk *d, int base, int **meta);
int		*rrb(t_chunk *d, int base, int **meta);
int		*rrr(t_chunk *d, int base, int **meta);

int		*n_ra(t_chunk *d, int base, int **meta);
int		*n_rb(t_chunk *d, int base, int **meta);
int		*n_rr(t_chunk *d, int base, int **meta);
int		*n_rra(t_chunk *d, int base, int **meta);
int		*n_rrb(t_chunk *d, int base, int **meta);
int		*n_rrr(t_chunk *d, int base, int **meta);

#endif
